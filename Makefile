ADOCFILES = $(wildcard *.adoc)
MDFILES   = $(patsubst %.adoc,%.md,$(ADOCFILES))
HTMLFILES = $(patsubst %.adoc,%.html,$(ADOCFILES))

all: $(MDFILES) $(HTMLFILES)

# convert Asciidoc to XML DocBook using asciidoctor
%.xml:%.adoc
	asciidoctor -b docbook5 $< -o $@

# Convert the DocBook to Github flavoured Markdown
# plus latex math with double backslash delimiter
# so we can use it withe mdBook
%.md:%.xml
	pandoc -f docbook $< -t markdown_github+tex_math_double_backslash -o $@

# Convert the Markdown to html using pandoc
# just to check if conversion is ok
%.html:%.md
	pandoc -s --katex -f markdown_github+tex_math_double_backslash $< -o $@

clean:
	rm -f $(MDFILES)
	rm -f $(HTMLFILES)
